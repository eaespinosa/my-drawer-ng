import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core'
import { NativeScriptCommonModule } from '@nativescript/angular'

import { SecondaryRoutingModule } from './secondary-routing.module'
import { SecondaryComponent } from './secondary.component'

@NgModule({
  imports: [NativeScriptCommonModule, SecondaryRoutingModule],
  declarations: [SecondaryComponent],
  schemas: [NO_ERRORS_SCHEMA],
})
export class SecondaryModule {}
