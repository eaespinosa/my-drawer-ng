import { Component, OnInit } from '@angular/core'
import { RadSideDrawer } from 'nativescript-ui-sidedrawer'
import { Application } from '@nativescript/core'
import { Button, EventData } from '@nativescript/core'

@Component({
  selector: 'Primary',
  templateUrl: './primary.component.html',
})
export class PrimaryComponent implements OnInit {
  constructor() {
    // Use the component constructor to inject providers.
  }

  ngOnInit(): void {
    // Init your component properties here.
  }

  onDrawerButtonTap(): void {
    const sideDrawer = <RadSideDrawer>Application.getRootView()
    sideDrawer.showDrawer()
  }

/*   onTap(args: EventData) {
    const button = args.object as Button
    // execute your custom logic here...
  } */
}
