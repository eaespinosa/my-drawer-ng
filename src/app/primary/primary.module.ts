import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core'
import { NativeScriptCommonModule } from '@nativescript/angular'

import { PrimaryRoutingModule } from './primary-routing.module'
import { PrimaryComponent } from './primary.component'

@NgModule({
  imports: [NativeScriptCommonModule, PrimaryRoutingModule],
  declarations: [PrimaryComponent],
  schemas: [NO_ERRORS_SCHEMA],
})
export class PrimaryModule {}
